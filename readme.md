# La Coordonnerie

Réalisation d'une API : application annuaire (dossier *annuaire*).  
Le projet Django s'appelle *mysite*

Le schema de la base de données concernant l'annuaire est situé dans le dossier *schemaDB*

L'API est en lien avec ce projet : https://framagit.org/mathieugodard/la-coordonnerie-frontend-vuejs
Vous pouvez consulter le rappport du projet sur le lien [Notion](https://www.notion.so/Projet-de-la-Coordonnerie-8d665e0232904ebc8a63d9ca50340524)

# Lancer l'API

## Installer l'environnement virtuel
```bash
$ python3 -m venv venv
```

## Activez l'environnement virtuel
```bash
$ source venv/bin/activate
```

## Cloner le projet
```bash
$ git clone git@framagit.org:mathieugodard/tutoriel-django.git
```
## Mettre à jour pip
```bash
(venv) ~$ python -m pip install --upgrade pip
```

## Installer tous les paquets

Dans le dossier *mysite* où se trouve le fichier *requirements.txt*, lancez :

```bash
(venv) ~$ pip install -r requirements.txt
```

## Configuration de la DB 

Installer PostgreSQL

Créer une Base de données PostreSQL nommée *api*
```bash
$ sudo -i -u postgres
```
```bash
$ createdb api
```
Dans le dossier mysite, ouvrez le fichier *settings.py* et modifiez dans l'option *DATABASES*,
en fonction de vos paramètres locaux, les clés *PASSWORD* et *USER*.

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'api',
        'USER': 'postgres',
        'PASSWORD': 'admin',
        'HOST': '127.0.0.1',
        'PORT': '5432',
        'TIME_ZONE': 'Europe/Paris'
    }
}
```

## Lancer la migration

Dans le dossier *mysite*, lancez :
```bash
(venv) ~$ python manage.py migrate
```

## Charger les données

Dans l'ordre :  

```bash
python manage.py loaddata users_new.json
python manage.py loaddata criterions_new.json
python manage.py loaddata needs_new.json
python manage.py loaddata organizations_new.json
```

## Créer un superutilisateur

```bash
$ python manage.py createsuperuser
Nom d’utilisateur (leave blank to use 'votreusername'):
Adresse électronique: votremail@zaclys.net
Password: motdepasse
Password (again): motdepasse
Superuser created successfully.
````

## Lancer le serveur
```bash
(venv) ~$ python manage.py runserver
```

## Accéder aux pages

http://127.0.0.1:8000/adminou/ (Pensez à vous connecter en tant que superuser)
http://127.0.0.1:8000/annuaire/ (application django non développée ici)  
http://127.0.0.1:8000/swagger/ (documentation endpoints)


# Modification des modèles

Si les modèles sont modifiés, lancez :
```bash
(venv) $ python manage.py makemigrations nom_de_l_application
```

Puis relancez :
```bash
(venv) ~$ python manage.py migrate
```