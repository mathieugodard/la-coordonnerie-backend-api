from django.contrib import admin

from .models import User, Organization, Criterion, Need


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display_links = ('id', 'username',)
    search_fields = ('username', 'first_name', 'last_name',)
    list_filter = ('address_city',)
    filter_horizontal = ('favorites_orga',)
    list_display = (
        'id',
        'username',
        'email',
        'first_name',
        'last_name',
        'address_zip',
        'address_city',
        'date_joined',
    )


@admin.register(Organization)
class OrganizationAdmin(admin.ModelAdmin):
    def address(self):
        return '{} {} {} {}'.format(self.address_number,
                                    self.address_street,
                                    self.address_zip,
                                    self.address_city)

    # TODO le retour chariot ne fonctionne pas.
    #  Idem avec la méthode str.format()
    def contact(self):
        cellphone = self.cellphone
        phone = self.phone
        email = self.email
        return f"{cellphone}\n{phone}\n{email}"

    search_fields = ('name',)
    list_display_links = ('name',)
    filter_horizontal = ('criterion', 'need')
    list_editable = ('validation',)
    list_display = ('id',
                    'name',
                    'short_description',
                    'owner',
                    address,
                    contact,
                    'website',
                    'siret',
                    'date_registration',
                    'date_modification',
                    'date_validation',
                    'validation'
                    )


@admin.register(Criterion)
class CriterionAdmin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(Need)
class NeedAdmin(admin.ModelAdmin):
    list_display = ('name',)
