from django.db import models
import uuid
from django.contrib.auth.models import AbstractUser
from django.template.defaultfilters import truncatechars


class Need(models.Model):
    name = models.CharField(max_length=50, default='')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = "Besoin"


class Criterion(models.Model):
    name = models.CharField(max_length=50, default='')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = "Critère"


class Organization(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    name = models.CharField(max_length=150, db_index=True)
    description = models.TextField(max_length=1000)
    type = models.CharField(max_length=30,
                            choices=[
                                ('Association', 'Association'),
                                ('Entreprise', 'Entreprise'),
                                ('Collectivité locale', 'Collectivité locale'),
                                ('Etat', 'Etat')])
    address_number = models.CharField(max_length=5, default='', blank=True)
    address_street = models.CharField(max_length=255, blank=True)
    address_zip = models.CharField(max_length=5)
    address_city = models.CharField(max_length=200)
    photo = models.ImageField(upload_to='profilOrganization/', blank=True,
                              max_length=150)
    cellphone = models.CharField(max_length=10, blank=True, default='')
    phone = models.CharField(max_length=10, blank=True, default='')
    email = models.EmailField(max_length=100)
    website = models.URLField(max_length=200, blank=True, null=True)
    date_registration = models.DateTimeField(auto_now_add=True)
    date_modification = models.DateTimeField(auto_now=True)
    date_validation = models.DateTimeField(null=True)
    validation = models.BooleanField(default=False)
    owner = models.ForeignKey('User', on_delete=models.CASCADE, default=0, blank=True, null=True)
    siret = models.CharField(max_length=14, unique=True, blank=True)
    criterion = models.ManyToManyField(Criterion, blank=True)
    need = models.ManyToManyField(Need, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-date_registration']
        verbose_name = "Organisation"

    @property
    def short_description(self):
        return truncatechars(self.description, 100)


class User(AbstractUser):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True,)
    address_zip = models.CharField(max_length=5, blank=True)
    address_city = models.CharField(max_length=200)
    photo = models.ImageField(upload_to='profilUser/', blank=True, max_length=150)
    favorites_orga = models.ManyToManyField(Organization, blank=True, default='')
    # date_registration = models.DateTimeField(auto_now_add=True)
    # username = models.CharField(max_length=100, unique=True, db_index=True)
    # email = models.EmailField(max_length=100, unique=True, db_index=True)
    # password = models.CharField(max_length=128)
    # first_name = models.CharField(max_length=100)
    # last_name = models.CharField(max_length=100)
    # role = models.CharField(max_length=100, default='user',
    #                         choices=[('user', 'user'),('admin', 'admin')])

    def __str__(self):
        return self.username

    REQUIRED_FIELDS = ['email', 'password', 'address_city']

    class Meta:
        ordering = ['-date_joined']
        verbose_name = "Utilisateur"
