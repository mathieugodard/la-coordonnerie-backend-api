from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
]

# A tester plus tard quand les authorisations seront ok
# from django.urls import path, include
# from rest_framework import routers
#
# router = routers.DefaultRouter()
# router.register('users', views.UserViewSet)
# router.register('organizations', views.OrganizationViewSet)
# router.register('criterions', views.CriterionViewSet)
# router.register('needs', views.NeedViewSet)
#
# urlpatterns = [
#     path('', include(router.urls))
# ]